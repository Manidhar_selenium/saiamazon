package Com.rediff.uills;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;


public class ExcelReder extends Common {

	public Object[][] readexceldata() throws EncryptedDocumentException, IOException {
		InputStream file=new FileInputStream(path+"\\src\\main\\resources\\TXTDATA.xlsx");
		Workbook workbook=WorkbookFactory.create(file);
		Sheet sheet=workbook.getSheetAt(0);
	int	lastrowNo =sheet.getLastRowNum();
	int totalrows=lastrowNo+1;
	
	System.out.println("totalRowNo: "+totalrows);
	Row row=sheet.getRow(0);
	int Totalcolumns=row.getLastCellNum();
	System.out.println("Totalcolumns: "+Totalcolumns);
	Object o[][]=new Object[totalrows][Totalcolumns];
	for (int i=0;i<totalrows;i++) {
		
		row =sheet.getRow(i);
		 Totalcolumns=row.getLastCellNum();
		 
		for(int j=0;j<Totalcolumns;j++) {
			
			Cell cell=row.getCell(j);
			o[i][j] =cell.getStringCellValue();
			System.out.println(o[i][j]);
			
		}
	}
	  return o;
	}
	
	
	public static void main(String[] args) throws EncryptedDocumentException, IOException {
		ExcelReder read=new ExcelReder();
		read.readexceldata();
	}

}
