package Com.rediff.uills;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Common {
	
	public  WebDriver driver = null;
	public Properties objects=null;
	public Properties Environment=null;
	String path=System.getProperty("user.dir");

public void openBrowser(){
	System.out.println("path: "+path);
	System.setProperty("webdriver.chrome.driver", path+"\\src\\main\\resources\\drivers\\chromedriver.exe");
	 driver=new ChromeDriver();
	//2.Navigate to https://www.rediff.com/
	driver.get(Environment.getProperty("url"));
	
	}


public void closeBrowser(){
	driver.close();
	}
public void LoadFiles() throws IOException{
	InputStream file=new FileInputStream(path+"\\src\\main\\resources\\Objectlocaters.properties");
	objects=new Properties();
	objects.load(file);
}
public void Clearfiles(){
	objects=null;
}
public void lodFile2() throws IOException {
	InputStream file2= new FileInputStream(path+"\\src\\main\\resources\\Environment.red.p");
	Environment=new Properties();
	Environment.load(file2);
}
	
		
	public  void CaptureScreenShot() throws IOException{
TakesScreenshot screen=(TakesScreenshot)driver;
		
		File  scrosefile= screen.getScreenshotAs(OutputType.FILE);
		File destinatuonFile=new File(path+"\\ScreenShort\\Screen1.png");
		FileUtils.copyFile(scrosefile, destinatuonFile);
		
		//C:\Users\Sai Nikhil\eclipse-workspace\selenium\Screenshorts
	}
		
		
	
	
}








